# Creati un program in care utilizatorul sa introduca un numar. Validati daca acest
# numar este par sau impar si afisati un raspuns in acest sens.


a = input("Introduceţi un număr: ")

comma = a.replace(",", ".")
count = comma.count(".")
digit = a.replace(".", "")

if count <= 1 and digit.isdigit():

    if (int(digit) % 2 > 0):
        print("Numarul introdus este un numar impar!")
        input("Apasati tasta <enter> pentru a iesi din program!")
    else:
        print("Numarul introdus este numar par!")
        input("Apasati tasta <enter> pentru a iesi din program!")

else:
    print("Şirul de caractere introdus de la tastatură nu este un număr întreg sau zecimal!")
    input("Apăsaţi tasta <enter> pentru a ieşi din program!")


