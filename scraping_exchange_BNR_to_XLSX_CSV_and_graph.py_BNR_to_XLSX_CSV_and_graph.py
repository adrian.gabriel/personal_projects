import requests
from bs4 import BeautifulSoup
import pandas as pd
import matplotlib.pyplot as plt
import csv

def grafic():
    keys_list = list(valori_tabel)                  # atributele pentru axa x
    values_list = list(valori_tabel.values())       # atributele pentru axa y
    df_project = pd.read_excel('C:/curs_valutar.xlsx')
    print(df_project)
    index_valuta = input('Introduceti indexul valutei: ')
    z = int(index_valuta)

# valori pentru axele x si y
    x = list()
    y = list()
    for i in range(2, int(len(keys_list))):
        x.append(keys_list[int(i)])
    for i in range(2, int(len(keys_list))):
        y.append(float((values_list[i][z]).replace(",", ".")))
    plt.plot(x, y, label=values_list[0][z])

# calcul procent intre prima si ultima valoare
    procent_zi = (float((values_list[-1][z]).replace(",", "."))) / 100 * (float((values_list[2][z]).replace(",", ".")))

# label vector x (creste sau scade)
    if (float((values_list[2][z]).replace(",", "."))) < (float((values_list[-1][z]).replace(",", "."))):
        plt.xlabel(f'{values_list[0][z]} ({values_list[1][z]}) a crescut cu {procent_zi:0.4f}%.')
    elif (float((values_list[2][z]).replace(",", "."))) > (float((values_list[-1][z]).replace(",", "."))):
        plt.xlabel(f'{values_list[0][z]} ({values_list[1][z]}) a scazut cu {procent_zi:0.4f}%.')

    plt.ylabel('Valoare')
    plt.title('Grafic curs valutar!')
    plt.legend()
    plt.show()

def enq_afisare_grafic():
    while True:
        answer = input('\nCursul valutar a fost salvat a adresa C:/curs_valutar.xlsx! \nCSV-ul, cu acelasi nume, a fost generat la adresa C:/curs_valutar.csv! \nDoresti sa vezi un grafic cu evolutia pe ultimele 5 zile al unei monede? (y/n)')
        if answer in ('y', 'Y', 'yes', 'YES', 'DA', 'da', 'Da', 'Yes', 'n', 'N', 'NO', 'NU', 'nu', 'Nu'):
            break
        print('Ati introdus o obtiune invalida!')
    if answer in ('y', 'Y', 'yes', 'YES', 'DA', 'da', 'Da', 'Yes'):
        grafic()
    elif answer in ('n', 'N', 'NO', 'NU', 'nu', 'Nu'):
        quit()

site = requests.get('https://www.bnr.ro/Cursul-de-schimb-524.aspx')

link = BeautifulSoup(site.text, 'html.parser')
header = ['denumire', 'abreviere']

valori_tabel = {'denumire': [], 'abreviere': []}
tabel = link.find_all('table', attrs={'class': 'cursTable'})
for i in tabel:
    for data in i.find_all('th'):
        if data != ' ':
            header.append(data.get_text().replace('\xa0', '.'))
        valori_tabel = {i: [] for i in header}
        while ' ' in header: header.remove(' ')
    for denumire in i.find_all('td', attrs={'class': 'c1'}):
        valori_tabel['denumire'].append(denumire.get_text().replace("\n", ""))
    for abreviere in i.find_all('td', attrs={'class': 'c2'}):
        if abreviere.get_text() != '':
            valori_tabel['abreviere'].append(abreviere.get_text())
del valori_tabel[' ']
table = link.find_all('tr')
for i in range(len(table)):
    valuta = table[i].find('td', attrs={'class': 'c2'})
    valoare = table[i].find_all('td')
    if (valuta != None):
        i_list = list(range(2, len(valoare)-1))
        for i in i_list:
            valori_tabel[header[int(i)]].append(valoare[len(valoare)-(len(valoare)-int(i))].get_text())

df = pd.DataFrame(valori_tabel, columns=header)
with pd.ExcelWriter('C:/curs_valutar.xlsx', engine='xlsxwriter') as writer:
    df.to_excel(writer, sheet_name='curs_valutar')
writer.save()

df = pd.read_excel('C:/curs_valutar.xlsx')
df.to_csv('C:/curs_valutar.csv', sep=";")

enq_afisare_grafic()
