import csv
import pandas as pd
from sqlalchemy import create_engine, types

engine = create_engine('mysql://root:829626@localhost/py_db')

df = pd.read_csv('xls_sql_csv.csv', encoding="utf-8")
df.to_sql('xls_sql_csv', con=engine, index=False, if_exists='replace')

pd.read_sql('SELECT * FROM xls_sql_csv', engine).to_excel('xls_sql_csv_sqlalchemy.xlsx', index=False)
pd.read_sql('SELECT * FROM xls_sql_csv', engine).to_csv('xls_sql_csv_sqlalchemy.csv', index=False)


# ----------------------------------------------------------------------------------------------------------------------

#--------------------------------------------- INTERACTIVE APP ---------------------------------------------------------

# import csv
# import pandas as pd
# from sqlalchemy import create_engine, types
# 
# username = input('Write database username: ')
# password = input(f'Write database password: ')
# database = input('Write database name: ')
# server = input('Input server name: ')
# csv_file = input('Input csv file name (without .csv): ')
# table_name = input('Input table name: ')
# xlsx_output = input('Input xlsx file name: ')
# csv_output = input('Input csv file name: ')
# 
# 
# engine = create_engine(f'mysql://{username}:{password}@{server}/{database}')
# 
# df = pd.read_csv(f'{csv_file}.csv', encoding="utf-8")
# df.to_sql(f'{table_name}', con=engine, index=False, if_exists='replace')
# 
# pd.read_sql(f'SELECT * FROM {table_name}', engine).to_excel(f'{xlsx_output}.xlsx', index=False)
# pd.read_sql(f'SELECT * FROM {table_name}', engine).to_csv(f'{csv_output}.csv', index=False)
