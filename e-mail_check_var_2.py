# Creati un program in care utilizatorul sa introduca o adresa de email de formatul litere_sau_cifre@litere_sau_cifre.litere.
# Validati acest sir de caractere si informati utilizatorul de raspuns. @ sau .(punct) trebuie sa exista o singura data in sirul de caractere


mail = input("Adresa de e-mail: ")

cs = (' ~+`!#$%^&*()=[-]{};\'"|,\/<>?')  # c_s - caractere_speciale
cs = list(cs)
mail_list = list(mail)
check = any(item in cs for item in mail_list)


class Exceptie_caractere(Exception):
    def _init_(self, raspuns):
        self.raspuns = raspuns

    def _str_(self):
        return self.raspuns


class Exceptie_punct_si_arond(Exception):
    def _init_(self, raspuns):
        self.raspuns = raspuns

    def _str_(self):
        return self.raspuns


class Exceptie_DNS(Exception):
    def _init_(self, raspuns):
        self.raspuns = raspuns

    def _str_(self):
        return self.raspuns


index1 = mail.index("@")
index2 = mail.index(".")

x = mail[int(index2) + 1:]

dif = int(index1) < (int(index2) + 1)

try:
    if check is True:
        raise Exceptie_caractere

    if mail.count(".") != 1 or mail.count("@") != 1 or dif is False:
        raise Exceptie_punct_si_arond
    domeniu = mail.split(".")
    x = mail[int(index2) + 1:]

    if x.isalpha() is False or len(x) < 2:
        raise Exceptie_DNS
except Exceptie_punct_si_arond:
    print("Adresa incorecta!")
except Exceptie_DNS:
    print("DNS invalid!")
except Exceptie_caractere:
    print("Adresa incorecta!")
else:
    print("Adresa este valida!")
