
class operatii(object):
    def adunare(self, nr_1, nr_2):
        suma = float(nr_1) + float(nr_2)
        if suma % 1 == 0:
            print("Suma este: {}".format(int(suma)))
        else:
            print("Suma este: {:0.2f}".format(suma))

    def scadere(self,nr_1, nr_2):
        diferenta = float(nr_1) - float(nr_2)
        if diferenta % 1 == 0:
            print("Diferenta este: {}".format(int(diferenta)))
        else:
            print("Diferenţa este: {:0.2f}".format(float(nr_1)-float(nr_2)))

    def inmultire(self, nr_1, nr_2):
        produs = float(nr_1) * float(nr_2)
        if produs % 1 == 0:
            print("Produsul este: {}".format(int(produs)))
        else:
            print("Produsul este: {:0.2f}".format(float(nr_1)*float(nr_2)))

    def impartire(self, nr_1, nr_2):
        cat = float(nr_1) / float(nr_2)
        if cat % 1 == 0:
            print("Câtul este: {}".format(int(cat)))
        else:
            print("Câtul este: {:0.2f}".format(float(nr_1)/float(nr_2)))

def main():

    expresie = input("Introduceti expresia (numar + operator + numar): ").replace(",", ".").replace(" ", "")
    expresie_lista = list(expresie)

    while True:
        try:
            if "+" in expresie_lista:
                if expresie_lista.count("+") == 1:
                    pass
                    termeni = (expresie.split("+"))
                    nr_1 = termeni[0]
                    if nr_1.count(".") <= 1:
                        pass
                    else:
                        print("Termenul 1 este gresit!")
                    nr_2 = termeni[1]
                    if nr_2.count(".") <= 1:
                        pass
                    else:
                        print("Termenul 2 este gresit!")
                    operatie = operatii()
                    operatie.adunare(nr_1, nr_2)
                else:
                    print("Numarul de operatori '+' este diferit de 1.")
            break
        except ValueError:
            print("Expresia introdusa este gresita!")
            break

    while True:
        try:
            if "-" in expresie_lista:
                if expresie_lista.count("-") == 1:
                    pass
                    termeni = (expresie.split("-"))
                    nr_1 = termeni[0]
                    if nr_1.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    nr_2 = termeni[1]
                    if nr_2.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    operatie = operatii()
                    operatie.scadere(nr_1, nr_2)
                else:
                    print("Numarul de operatori '-' este diferit de 1.")
            break
        except ValueError:
            print("Expresia introdusa este gresita!")
            break

    while True:
        try:
            if "*" in expresie_lista:
                if expresie_lista.count("*") == 1:
                    pass
                    termeni = (expresie.split("*"))
                    nr_1 = termeni[0]
                    if nr_1.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    nr_2 = termeni[1]
                    if nr_2.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    operatie = operatii()
                    operatie.inmultire(nr_1, nr_2)
                else:
                    print("Numarul de operatori '*' este diferit de 1.")
            break
        except ValueError:
            print("Expresia introdusa este gresita!")
            break
    while True:
        try:
            if "/" in expresie_lista:
                if expresie_lista.count("/") == 1:
                    pass
                    termeni = (expresie.split("/"))
                    nr_1 = termeni[0]
                    if nr_1.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    nr_2 = termeni[1]
                    if nr_2.count(".") <= 1:
                        pass
                    else:
                        print("Termen gresit!")
                    operatie = operatii()
                    operatie.impartire(nr_1, nr_2)
                else:
                    print("Numarul de operatori '/' este diferit de 1.")
            break
        except ValueError and ZeroDivisionError:
            print("Expresia introdusa este gresita!")
            break

if __name__ == "__main__":
    main()

restart = input("\nCalcul nou? \n")
if restart in ["da", "d", "yes", "y", "si", "ja"]:
    main()
else:
    exit()
