
class Exceptie_caractere(Exception):
    def _init_(self, caracter):
        self.caracter = caracter

    def _str_(self):
        return self.caracter
def main():
    from getpass import getpass
    print("ATENŢIE!!! Tastaţi cu grija, nu veti vedea cuvantul introdus!")
    get = list(getpass("Introduceti cuvantul: "))
    cuvant = [x.lower() for x in get]
    #cuvant = input("Introduceti cuvantul: ").lower()
    lista_nula = []
    cuvant_1 = list(cuvant[1:-1])

    validare = " 0123456789~+`!#$%^&*()=[-]{};\'\"|,\/<>?" # "aăâbcdefghiîjklmnopqrsştţuvwxzy"
    validare_lista = list(validare)

    litere_gresite = list()
    litere_corecte = [cuvant[0], cuvant[-1]]

    index_start = []
    index_end = []

    check = any(k in cuvant for k in validare_lista)

    try:
        if check is True:
            raise Exceptie_caractere


        # inlocuieste element lista cuvant cu _
        for litera in cuvant_1:
            lista_nula.append("_")

        # verificare daca prima litera este in cuvant
        for idx, m in enumerate(cuvant_1):
            if m == cuvant[0]:
                index_start.append(idx)
        print("indexul literei de start in cuvant este: {}".format(index_start))

        for m in index_start:
            lista_nula[m] = cuvant[0]
        print(lista_nula)
        # pana aici verificare daca prima litera este in cuvant

        # verificare daca ultima litera este in cuvant
        for idx, n in enumerate(cuvant_1):
            if n == cuvant[-1]:
                index_end.append(idx)
        print("indexul literei de start in cuvant este: {}".format(index_end))

        for n in index_end:
            lista_nula[n] = cuvant[-1]
        print(lista_nula)
        # pana aici verificare daca ultima litera este in cuvant

        # definim incercarile
        incercari = 0
        nr_max_incercari = 8

        print("Sa incepem!\n")

        sfarsit = False

        while not sfarsit:
            print(f"\nMai ai {nr_max_incercari - incercari} incercari")
            print(f"Cuvantul este: {cuvant[0] + ' ' + ' '.join(lista_nula) + ' ' + cuvant[-1]} ")

            print("   _________")
            print("  |         |")
            print("  |         " + ("O" if incercari > 0 else""))
            # print("  |         " + ("|" if incercari > 1 else ""))
            if incercari < 2:
                print("  |         ")
            elif incercari == 2:
                print("  |        /")
            elif incercari > 2:
                print("  |        / \\")
            print("  |         " + ("|" if incercari > 3 else ""))
            if incercari < 5:
                print("  |         ")
            elif incercari == 5:
                print("  |        /")
            elif incercari > 5:
                print("  |        / \\")
            if incercari < 7:
                print("  |      ______  ")
            elif incercari == 7:
                print("  |      \  ___  ")
            elif incercari == 8:
                print("  |      \    /  ")
            if incercari < 7:
                print("  |      |    |  ")
            elif incercari == 7:
                print("  |      |\   |  ")
            elif incercari == 8:
                print("  |      |\  /|  " + "  BANG! " if incercari == 8 else "")
            print("__|______|____|__ ")


            incercare = input("Incearca-ti norocul: ")

            if len(incercare) != 1:
                print(f"Ati introdus mai multe caractere: {incercare}. Reincearca, te rog!")
                print(f"Lista de litere gresite este: {litere_gresite}.")

            elif incercare in cuvant_1:
                litere_corecte.append(incercare)
                print(f"Litera introdusa este corecta! {incercare} este in cuvant!")
                print(f"Lista de litere gresite este: {litere_gresite}.")
                for i in range(len(cuvant_1)):
                    litera = cuvant_1[i]
                    if litera == incercare:
                        lista_nula[i] = cuvant_1[i]
                        cuvant_1[i] = "_"

            elif incercare in litere_gresite or incercare in litere_corecte:
                print(f"Ati mai introdus aceasta litera {incercare}. Reincercati!")
                print(f"Lista de litere gresite este: {litere_gresite}.")

            else:
                litere_gresite.append(incercare)
                print(f"Litera introdusa este gresita! {incercare} nu este in cuvant!")
                print(f"Lista de litere gresite este: {litere_gresite}.")
                incercari += 1

        # sfarsit de loop, Ai castigat
            if all(i in litere_corecte for i in cuvant):
                print(f"\n\n\tAi castigat!\nCuvantul este {' '.join(cuvant)}.")
                sfarsit = True

            # sfarsit de loop, spanzuratoare finala
            if incercari >= nr_max_incercari:
                print("\nN-ai avut noroc, poate data viitoare!")
                print("   _________")
                print("  |         |")
                print("  |         O")
                print("  |        / \\")
                print("  |         |")
                print("  |        / \\")
                print("  |      \     /  ")
                print("  |      |\   /|  " + "  BANG! " if incercari == 8 else "")
                print("__|______|_____|__ ")
                sfarsit = True

    except Exceptie_caractere:
        print("Cuvantul contine caractere care nu sunt litere!")

if __name__ == "__main__":
    main()

restart = input("Vrei sa mai incerci odata? \n")
if restart in ["da", "yes", "y"]:
    main()
else:
    exit()
