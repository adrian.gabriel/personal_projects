# Sa se verifice daca textul introdus de la tastatura de catre un utilizator este un sir de
# caractere de tip string sau un sir de numere. Utilizati instructiunea de tip if-elif-else
# si afisati un mesaj cu rezultatul.

a = input("Cum te numeşti: ")
b = input("Introduceţi un şir de caractere: ")

count = b.count(".")

if count < 1 and b.isdigit():
    print("{}, şirul de caractere introdus de la tastatură este un număr întreg!" .format(a))
    input("Apăsaţi tasta <enter> pentru a ieşi din program!")

elif count == 1 and b.replace(".", "").isdigit():
    print("{}, şirul de caractere introdus de la tastatură este un număr zecimal!" .format(a))
    input("Apăsaţi tasta <enter> pentru a ieşi din program!")

else:
    print("{}, şirul de caractere introdus de la tastatură este unul de tip string!".format(a))
    input("Apăsaţi tasta <enter> pentru a ieşi din program!")

