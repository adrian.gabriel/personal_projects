import pandas as pd
import xlsxwriter
import mysql.connector
import csv

data = pd.read_excel(r'C:\Users\Python\PycharmProjects\database_manipulation\database_insert_bnr.xlsx', sheet_name='BNR_selenium')
df = pd.DataFrame(data)

df_columns = list(pd.DataFrame(data, columns=df.columns[1:]))
df_rows = df.values.tolist()

conn = mysql.connector.connect(user='root', password='829626', host='127.0.0.1')
cursor = conn.cursor()
cursor.execute("DROP database IF EXISTS py_db")
sql = "CREATE database py_db"
cursor.execute(sql)

conn = mysql.connector.connect(
    user='root', password='829626', host='127.0.0.1', database='py_db', auth_plugin='mysql_native_password')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS XLS_SQL_CSV")

sql = f'''CREATE TABLE XLS_SQL_CSV(
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id)
)'''
cursor.execute(sql)

list_df_columns = []
for column_name in df_columns:
    sql = f'''ALTER TABLE XLS_SQL_CSV ADD ({column_name.replace(" ", "_")} VARCHAR (50))'''
    list_df_columns.append(column_name.replace(" ", "_"))
    cursor.execute(sql)
table_header = ', '.join(list_df_columns)

for row in range(len(df_rows)):
    sql = f"""INSERT INTO XLS_SQL_CSV
        ({(table_header)})
        VALUES {tuple(df_rows[row][1:])}
        """
    try:
        cursor.execute(sql)
        conn.commit()
    except:
        conn.rollback()

pd.read_sql('SELECT * FROM xls_sql_csv', conn).to_excel('xls_sql_csv.xlsx', index=False)
pd.read_sql('SELECT * FROM xls_sql_csv', conn).to_csv('xls_sql_csv.csv', index=False)
