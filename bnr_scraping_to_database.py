import sys
from selenium import webdriver
import pandas as pd
import xlsxwriter
import mysql.connector
import csv

def chunk_based_on_size(lst, n):
    for x in range(0, len(lst), n):
        each_chunk = lst[x: n+x]

        if len(each_chunk) < n:
            each_chunk = each_chunk + [None for y in range(n-len(each_chunk))]
        yield each_chunk

#-----------------------------------------------------------------------------------------------------------------------

browser = webdriver.Chrome(executable_path=r'C:\Users\Python\AppData\Local\Programs\Python\Python38\Lib\site-packages\chromedriver\chromedriver.91.exe')
browser.get("https://www.bnr.ro/Cursul-de-schimb-524.aspx")
table = browser.find_element_by_xpath('//*[@id="contentDiv"]/span[2]/table')
txt = open('database_insert_bnr.txt', 'w+', encoding="utf-8")
txt.writelines(table.text)
txt.close()
rows_list = table.text.split('\n')

header_list = rows_list[0].replace('.', ' ').split(' ')

cap_tabel = ['Denumire', 'Abreviere']
day_columns = list(chunk_based_on_size(header_list, 3))
for day in range(len(day_columns)):
    cap_tabel.append(" ".join(day_columns[day]))

rows_list.pop(0)
dicitonar_1 = {}
counter = 0
for element in rows_list:
    lista_1 = []
    lista_1.append(' '.join(element.split(' ')[:-6]))
    for x in element.split(' ')[-6:]:
        lista_1.append(x)
    dicitonar_1[counter] = lista_1
    counter += 1

len_dictionar_1 = len(dicitonar_1)

df = pd.DataFrame.from_dict(dicitonar_1, orient='index', columns=cap_tabel)

with pd.ExcelWriter('database_insert_bnr.xlsx', engine='xlsxwriter') as writer:
    df.to_excel(writer, sheet_name='BNR_selenium')

browser.close()

conn = mysql.connector.connect(
    user='root', password='829626', host='127.0.0.1', database='py_db', auth_plugin='mysql_native_password')

cursor = conn.cursor()

cursor.execute("DROP TABLE IF EXISTS BNR")

sql =f'''CREATE TABLE BNR(
       {cap_tabel[0].replace('"', "")} CHAR(50) NOT NULL,
       {cap_tabel[1].replace('"', "")} CHAR(5),
       {cap_tabel[2].replace('"', "").replace(" ","_")} FLOAT,
       {cap_tabel[3].replace('"', "").replace(" ","_")} FLOAT,
       {cap_tabel[4].replace('"', "").replace(" ","_")} FLOAT,
       {cap_tabel[5].replace('"', "").replace(" ","_")} FLOAT,
       {cap_tabel[6].replace('"', "").replace(" ","_")} FLOAT
    )'''
cursor.execute(sql)

for data_raws in range(len_dictionar_1):
    sql = f"""INSERT INTO BNR(
        {cap_tabel[0].replace('"', "")},
        {cap_tabel[1].replace('"', "")},
        {cap_tabel[2].replace('"', "").replace(" ", "_")},
        {cap_tabel[3].replace('"', "").replace(" ", "_")},
        {cap_tabel[4].replace('"', "").replace(" ", "_")},
        {cap_tabel[5].replace('"', "").replace(" ", "_")},
        {cap_tabel[6].replace('"', "").replace(" ", "_")})
        VALUES('{dicitonar_1[data_raws][0]}', '{dicitonar_1[data_raws][1]}', {dicitonar_1[data_raws][2].replace(",", ".")},
                {dicitonar_1[data_raws][3].replace(",", ".")}, {dicitonar_1[data_raws][4].replace(",", ".")},
                {dicitonar_1[data_raws][5].replace(",", ".")}, {dicitonar_1[data_raws][6].replace(",", ".")})
        """
    try:
        cursor.execute(sql)
        conn.commit()

    except:
        conn.rollback()

QUERY = 'SELECT * FROM py_db.bnr;'

cursor.execute(QUERY)
result = cursor.fetchall()

write_csv = csv.writer(open('database_insert_bnr.csv', 'w', encoding="utf-8", newline=''))
for csv_row in result:
    write_csv.writerow(csv_row)
