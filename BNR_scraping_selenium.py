# IMPORTANT!!!
# link-ul cu driver-ul chrome trebuie sa aiba aceeasi versiune cu Chrome Browser-ul instalat


from selenium import webdriver
import pandas as pd

browser = webdriver.Chrome(r'C:\Users\Latitude\AppData\Local\Programs\Python\Python37-32\Lib\site-packages\chromedriver\chromedriver.88.exe')
# change chromedriver link before try the program
browser.get("https://www.bnr.ro/Cursul-de-schimb-524.aspx")
table = browser.find_element_by_xpath('//*[@id="contentDiv"]/span[2]/table')
fisier = open('BNR_scraping_selenium.txt', 'w+', encoding="utf-8")
fisier.writelines(table.text)
fisier.close()
dictionar = list()
dictionar.append(table.text)
lista = table.text.split('\n')

lista_header = lista[0].replace('.', ' ').split(' ')
b = []
a = ''
b.append(a)
for i in range(0, len(lista_header)+3):
    try:
        if i % 3 == 0:
            b.append(a)
            a = ''
            a = lista_header[int(i)]
        else:
            a += f' {str(lista_header[i])}'
    except IndexError:
        pass
cap_tabel = ['Denumire', 'Abreviere']
for j in range(2):
    b[j] = cap_tabel[j]

lista.pop(0)
dicitonar_1 = {}
counter = 0
for element in lista:
    lista_1 = []
    lista_1.append(' '.join(element.split(' ')[:-6]))
    for x in element.split(' ')[-6:]:
        lista_1.append(x)
    dicitonar_1[counter] = lista_1
    counter += 1

df = pd.DataFrame.from_dict(dicitonar_1, orient='index', columns=b)

with pd.ExcelWriter('BNR_scraping_selenium.xlsx', engine='xlsxwriter') as writer:
    df.to_excel(writer, sheet_name='BNR_selenium')
    # for BNR_scraping_selenium in writer.sheets.values():
    #     BNR_scraping_selenium.set_column(2, len(b), 10)

    def autosize_excel_columns(worksheet, df):
        autosize_excel_columns_df(worksheet, df.index.to_frame())
        autosize_excel_columns_df(worksheet, df, offset=df.index.nlevels)

    def autosize_excel_columns_df(worksheet, df, offset=0):
        for idx, col in enumerate(df):
            series = df[col]
            max_len = max((
                series.astype(str).map(len).max(),
                len(str(series.name))
            )) + 1
            worksheet.set_column(idx + offset, idx + offset, max_len)

    workbook = writer.book
    worksheet = writer.sheets['BNR_selenium']
    value_columns = workbook.add_format({'align': 'center', 'italic': True, 'font_color': '#9C0006'})
    worksheet.set_column('C:H', autosize_excel_columns(worksheet, df), value_columns)
    money_name = workbook.add_format({'italic': True, 'bold': True, 'font': 'Times new roman'})
    worksheet.set_column('B:B', autosize_excel_columns_df(worksheet, df, offset=1), money_name)

    sheetname = 'BNR_selenium'
    df.to_excel(writer, sheet_name='BNR_selenium', freeze_panes=(df.columns.nlevels, df.index.nlevels))
    # worksheet = writer.sheets['BNR_selenium']
    autosize_excel_columns(worksheet, df)

writer.save()
browser.close()
