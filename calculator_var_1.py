print("Calculator \nSe pot executa următoarele operaţii: + , - , * , : , ** (ridicare la putere) \n")

def formatNumber(number):
    if number % 1 == 0:
        return int(number)
    else:
        return number

def calc():
    var_2_list = ["+", "-", "/", "*", "**"]

    while True:
        try:
            var_1 = float(input("Introduceţi prima variabilă şi apăsaţi <enter>: "))
            break
        except ValueError:
            print("Caracterul(ele) introdus(e) nu este(sunt) un număr! Reintroduceţi prima varialbilă! ")

    while True:
        var_2 = str(input("Introduceţi operaţia, apoi apăsaţi <enter>: "))
        if var_2 in var_2_list:
            break

    while True:
        try:
            var_3 = float(input("Introduceţi a doua variabilă şi apăsaţi <enter>: "))
            break
        except ValueError:
            print("Caracterul(ele) introdus(e) nu este(sunt) un număr! Reintroduceţi a doua variabilă: ")

    while var_2 == "+":
        print(formatNumber(var_1) + formatNumber(var_3))
        break

    while var_2 == "-":
        print(formatNumber(var_1) - (formatNumber(var_3)))
        break

    while var_2 == "*":
        print(formatNumber(var_1) * formatNumber(var_3))
        break

    while var_2 == "/":
        print(formatNumber(var_1) / formatNumber(var_3))
        break

    while var_2 == "**":
        print(formatNumber(var_1) ** formatNumber(var_3))
        break

    quit = input("Apasă tasta <enter> pentru a ieşi! ")


calc()