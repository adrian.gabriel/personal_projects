import datetime
cnp = input('Check the cnp: ')

def check_cnp(cnp):
    if len(cnp) != 13 or not cnp.isdigit():
        print('Maximum number of digits is 13!')
    s = int(cnp[0])
    if s == 0:
        print("Invalid CNP!")
    date = f"{cnp[3:5]}/{cnp[5:7]}/{cnp[1:3]}"

    try:
        datetime.datetime.strptime(date, '%m/%d/%y')
    except ValueError:
        print("Invalid date!")
    if datetime.datetime.strftime(datetime.datetime.strptime(date, '%m/%d/%y'), '%m/%d/%y') > datetime.date.today().strftime('%m/%d/%y'):
        print('He has not yet been born!')
    jj = int(cnp[7:9])
    if jj not in range(1, 47) and jj not in ['51', '52']:
        print('Invalid county!')
    nnn = int(cnp[9:12])
    if nnn == '000':
        print('Born number invalid')
    c = int(cnp[12])
    cnp = list(map(int, list(cnp)))
    CONTROL_NUMBER = "279146358279"
    control_list = list(map(int, list(CONTROL_NUMBER)))
    c = cnp[12]
    check = sum(x * y for x, y in zip(cnp[:12], control_list)) % 11
    if check == 10:
        check = 1
    if check != c or (check == 10 and c != 1):
        print("Invalid control number.")

check_cnp(cnp)